============
Installation
============

At the command line::

    $ easy_install rxcheck-base-importer

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv rxcheck-base-importer
    $ pip install rxcheck-base-importer