===============================
RxCheck Base Importer
===============================

.. image:: https://badge.fury.io/py/rxcheck-base-importer.png
    :target: http://badge.fury.io/py/rxcheck-base-importer
    
.. image:: https://travis-ci.org/randlet/rxcheck-base-importer.png?branch=master
        :target: https://travis-ci.org/randlet/rxcheck-base-importer

.. image:: https://pypip.in/d/rxcheck-base-importer/badge.png
        :target: https://pypi.python.org/pypi/rxcheck-base-importer


Base RxCheck package for implementing treatment planning system importers

* Free software: BSD license
* Documentation: http://rxcheck-base-importer.rtfd.org.

Features
--------

* TODO