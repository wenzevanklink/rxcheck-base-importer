#!/usr/bin/env python
# -*- coding: utf-8 -*-
import dvh

def get_subclasses(c):
    subclasses = c.__subclasses__()
    for d in list(subclasses):
        subclasses.extend(get_subclasses(d))
    return subclasses

class BaseImporter(object):
    """
    Minimal interface for implementing an RxCheck treatment plan importer.
    Your derived importer must implement get_patients and get_patient_plan
    described below.
    """

    def get_patients(self):
        """
        patients returns an iterable of dicts containting patient info
        for all patients available from this treatment planning system
        importer.

        The form of each dict is:

        {
            patient_id: "0123456789",
            plans: [
                {
                   "name": "Plan 1",
                   "id": "1111111111",
                   "plan_date": datetime.datetime,
                },
                {
                   "name": "Plan 2",
                   "id": "2222222222",
                   "plan_date": datetime.datetime,
                }
                ...
            ]
        }

        To be implemented by end user.
        """
        raise NotImplementedError


    def get_patient_plan(self, patient_id, plan_id):
        """
        return a dict of data about the requested plan. The format
        of the dict is as follows:

        plan_data = {
            "patient_id": "0123456789",
            "name": "Plan Name",
            "id": "plan_id_1234",
            "structures": [
                {
                    "name": "Heart",
                    "volume": 1234.5, # in cc's
                    "dvh": {

                        # dose points  in cGy,
                        "doses": [0, 10, 20, 30, 40, 50],

                        # volumes (cumulative or differential) for each bin
                        # there must be len(volumes) == len(dose_bins)
                        "volumes": [0, 100, 20, 200, 10]
                    }
                },
                ...
                {
                    "name": "PTV",
                    ...
                }
            ]
        }
        """

        raise NotImplementedError

    @classmethod
    def _get_name(cls):
        try:
            return cls.NAME
        except AttributeError:
            raise AttributeError("Your importer class must have a NAME attribute defined ")

    @classmethod
    def _get_handle(cls):
        try:
            return cls.HANDLE
        except AttributeError:
            raise AttributeError("Your importer class must have a HANDLE attribute")

    @classmethod
    def _get_importers(cls):
        return dict((x._get_handle(), x) for x in get_subclasses(cls))

    def _patients(self):
        return self.get_patients()

    def _patient_plan(self, patient_id, plan_id):
        return self._process_plan(self.get_patient_plan(patient_id, plan_id))

    def _process_plan(self, plan):
        """add some useful metrics like structure min/max/mean doses"""

        for structure in plan["structures"]:
            dvh_dat = dvh.DVH(structure["dvh"]["doses"], structure["dvh"]["volumes"])
            structure.update(dvh_dat.to_dict(with_diff=False))

        return plan



